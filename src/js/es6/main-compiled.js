"use strict";

(function () {
    function search(offset) {
        var query_val = $('#s-box--input').val();

        if (query_val !== '') {
            (function () {
                var page = 'http://en.wikipedia.org/?curid=';
                var wikiURL = "http://en.wikipedia.org/w/api.php?format=json&action=query&generator=search&gsrnamespace=0&gsrlimit=10&prop=pageimages|extracts&pilimit=max&exintro&explaintext&exsentences=1&exlimit=max&gsrsearch=" + query_val;

                if (Boolean(offset)) {
                    console.log(offset);
                    wikiURL += "&gsroffset=" + offset;
                }
                wikiURL += "&callback=JSON_CALLBACK";
                $.ajax({
                    url: wikiURL,
                    type: 'GET',
                    dataType: "jsonp",

                    success: function success(data) {
                        var results = '';
                        console.log(data);
                        for (var i in data.query.pages) {
                            if (data.query.pages[i].extract.match(/may refer to:/g)) {
                                continue;
                            }

                            results += "<div class='card'><a target='_blank' href='" + page + data.query.pages[i].pageid + "'><span class='card-header'>" + data.query.pages[i].title + "</span></a>";
                            results += "<div class='card-body'>" + data.query.pages[i].extract + "</div>";
                            results += "</div>";
                        }

                        results += '<div class="btn--group"><a class="btn btn-previous"><i class="material-icons">navigate_before</i></a>' + '<a class="btn btn-next"><i class="material-icons">navigate_next</i></a></div>';

                        $('.results').html(results).slideDown(400);
                        $('.btn-next').click(function () {
                            console.log("click");

                            off += 10;
                            search(off);
                        });

                        $('.btn-previous').click(function () {
                            console.log("click-back");
                            if (off >= 10) {
                                off -= 10;
                                search(off);
                            }
                        });
                    }
                });
            })();
        }
    }
    var off = 0;
    function clear() {
        $('#s-box--input').val([]);
        $('.results').html('').slideUp(400);
    }

    $('#s-box--input').keypress(function (e) {
        if (e.keyCode == 13) {
            search();
        }
    });

    $('.btn-search').click(function () {
        search();
    });

    $('#clear').click(function () {
        clear();
    });
})();

//# sourceMappingURL=main-compiled.js.map